package ru.chernyavskiy.dto;

import lombok.Data;

@Data
public class ShareholderSaveRequest {
    private String firstName;
    private String secondName;
    private String lastName;
}
