package ru.chernyavskiy.dto;

import lombok.Data;

@Data
public class StockEditRequest {
    private Long id;
    private String stockName;
    private Double currentValue;
}
