package ru.chernyavskiy.dto;

import lombok.Data;
import ru.chernyavskiy.model.Shareholder;
import ru.chernyavskiy.model.Stock;

import java.util.Date;

@Data
public class OrderDto {
    private Long orderId;

    private Shareholder shareholder;

    private Stock stock;

    private Double purchasePrise;

    private Integer purchaseNumber;

    private Date purchaseDate;
}
