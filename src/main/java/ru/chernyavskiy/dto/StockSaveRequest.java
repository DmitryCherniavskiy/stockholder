package ru.chernyavskiy.dto;

import lombok.Data;

@Data
public class StockSaveRequest {
    private String stockName;
    private Double currentValue;
}
