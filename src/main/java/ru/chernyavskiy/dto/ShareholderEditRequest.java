package ru.chernyavskiy.dto;

import lombok.Data;

@Data
public class ShareholderEditRequest {
    private Long id;
    private String firstName;
    private String secondName;
    private String lastName;
}
