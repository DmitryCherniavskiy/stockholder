package ru.chernyavskiy.repository;

import org.springframework.stereotype.Repository;
import ru.chernyavskiy.model.Shareholder;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

@Repository
public interface ShareholderRepository extends CrudRepository<Shareholder, Long> {
    Optional<Shareholder> findById(Long id);
}