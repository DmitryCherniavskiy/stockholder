package ru.chernyavskiy.repository;

import org.springframework.stereotype.Repository;
import ru.chernyavskiy.model.User;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    User findByLogin(String login);

}
