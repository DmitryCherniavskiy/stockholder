package ru.chernyavskiy.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.chernyavskiy.dto.ShareholderEditRequest;
import ru.chernyavskiy.dto.StockEditRequest;
import ru.chernyavskiy.dto.StockSaveRequest;
import ru.chernyavskiy.service.SecurityService;
import ru.chernyavskiy.service.StockService;

@RestController
@RequestMapping("/public/rest/stocks")
public class StockRestController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private StockService stockService;

    @GetMapping
    public ResponseEntity<Object> browse() {
        return ResponseEntity.ok(stockService.listAll());
    }

    @DeleteMapping
    public void delete(@RequestParam("id") Long id) {

        if (!securityService.getAuthorizedUserName().equals("Admin")) {
            throw new ForbiddenException();
        }
        stockService.delete(id);
    }

    @PostMapping
    public ResponseEntity<Object> add(
            @RequestBody StockSaveRequest stock) {
        if (!securityService.getAuthorizedUserName().equals("Admin")) {
            throw new ForbiddenException();
        }
        return ResponseEntity.ok(stockService.add(stock.getStockName(), stock.getCurrentValue()));
    }

    @PutMapping
    public ResponseEntity<Object> edit(
            @RequestBody StockEditRequest stock) {
        if (!securityService.getAuthorizedUserName().equals("Admin")) {
            throw new ForbiddenException();
        }
        return ResponseEntity.ok(stockService.edit(stock.getId(), stock.getStockName(), stock.getCurrentValue()));
    }

}
