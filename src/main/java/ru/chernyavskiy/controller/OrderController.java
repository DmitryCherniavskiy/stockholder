package ru.chernyavskiy.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.chernyavskiy.model.Order;
import ru.chernyavskiy.service.OrderService;
import ru.chernyavskiy.service.ShareholderService;
import ru.chernyavskiy.service.StockService;

import java.security.Principal;
import java.util.Date;

@Controller
public class OrderController {

    @Autowired
    StockService stockService;

    @Autowired
    ShareholderService shareholderService;

    @Autowired
    OrderService orderService;

    @GetMapping(value = "/orders")
    public ModelAndView getAllOrders() {
        ModelAndView mav = new ModelAndView("orders");
        mav.addObject("orders", orderService.listAll());
        return mav;
    }

    @GetMapping(value = "/orders/shareholder/{id}")
    public ModelAndView getOrdersByShareholders(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("orders");
        mav.addObject("orders", orderService.findByShareholder(id));
        mav.addObject("shareholder", shareholderService.findById(id));
        mav.addObject("stocks", stockService.listAll());
        return mav;
    }

    @GetMapping(value = "/orders/{id}")
    public ModelAndView getOrderById(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("orders");
        mav.addObject("orders", orderService.findByShareholder(id));
        return mav;
    }

    @PostMapping(value = "/orders")
    public String add(
            @RequestParam("shareholder_id") Long shareholderId,
            @RequestParam("stock_id") Long stockID,
            @RequestParam("purchase_price") Double purchasePrice,
            @RequestParam("purchase_number") Integer purchaseNumber) {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            throw new ForbiddenException();
        }
        Order order = new Order();
        order.setPurchasePrise(purchasePrice);
        order.setPurchaseNumber(purchaseNumber);
        order.setStock(stockService.findById(stockID));
        order.setShareholder(shareholderService.findById(shareholderId));
        order.setPurchaseDate(new Date());
        orderService.save(order);
        return "redirect:/orders/shareholder/" + shareholderId;
    }

    @DeleteMapping(value = "/orders")
    public String delete(@RequestParam("id") Long id) {
        Long shareholderId = orderService.findById(id).getShareholder().getId();
        orderService.delete(id);
        return "redirect:/orders/shareholder/" + shareholderId;
    }
}
