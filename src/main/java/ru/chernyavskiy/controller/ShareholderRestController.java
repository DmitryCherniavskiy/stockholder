package ru.chernyavskiy.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.chernyavskiy.dto.ShareholderEditRequest;
import ru.chernyavskiy.dto.ShareholderSaveRequest;
import ru.chernyavskiy.service.SecurityService;
import ru.chernyavskiy.service.ShareholderService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/public/rest/shareholders")
public class ShareholderRestController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private ShareholderService shareholderService;

    @GetMapping
    public ResponseEntity<Object> browse() {
        return ResponseEntity.ok(shareholderService.listAll());
    }

    @DeleteMapping
    public void delete(@RequestParam("id") Long id) {
        if (!securityService.getAuthorizedUserName().equals("Admin")) {
            throw new ForbiddenException();
        }
        shareholderService.delete(id);
    }

    @PostMapping
    public ResponseEntity<Object> save(
            @RequestBody ShareholderSaveRequest shareholder) {
        if (!securityService.getAuthorizedUserName().equals("Admin")) {
            throw new ForbiddenException();
        }
        return ResponseEntity.ok(shareholderService.add(shareholder.getFirstName(), shareholder.getSecondName(), shareholder.getLastName()));
    }

    @PutMapping
    public ResponseEntity<Object> edit(
            @RequestBody ShareholderEditRequest shareholder) {
        if (!securityService.getAuthorizedUserName().equals("Admin")) {
            throw new ForbiddenException();
        }
        return ResponseEntity.ok(shareholderService.edit(shareholder.getId(), shareholder.getFirstName(), shareholder.getSecondName(), shareholder.getLastName()));
    }

}
