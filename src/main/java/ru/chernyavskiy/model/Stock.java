package ru.chernyavskiy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "stock")
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stock_uid")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "currentValue")
    private Double currentValue;

    @OneToMany(targetEntity = Order.class, mappedBy = "stock" , fetch = FetchType.LAZY)
    private List<Order> ordersList;

    public Stock(String name, Double currentValue) {
        this.name = name;
        this.currentValue = currentValue;
    }
}
