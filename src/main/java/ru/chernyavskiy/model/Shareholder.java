package ru.chernyavskiy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "shareholder")
public class Shareholder implements Serializable {
    private static final long serialVersionUID = 1L;

    public Shareholder(String fn, String sn, String ln)
    {
        this.firstName = fn;
        this.secondName = sn;
        this.lastName = ln;
    }

    @Id
    @Column(name = "shareholder_uid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany(targetEntity = Order.class, mappedBy = "shareholder" , fetch = FetchType.LAZY)
    private List<Order> ordersList;
}
