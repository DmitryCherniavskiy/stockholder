package ru.chernyavskiy.service;

import ru.chernyavskiy.dto.OrderDto;
import ru.chernyavskiy.model.Order;
import ru.chernyavskiy.model.Stock;

import java.util.List;

public interface OrderService {
    List<OrderDto> findByShareholder(Long shareholderId);

    Order findById(Long stockId);

    Iterable<Order> listAll();

    void delete(Long id);

    Order save(Order order);
}
