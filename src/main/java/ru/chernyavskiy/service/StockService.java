package ru.chernyavskiy.service;

import ru.chernyavskiy.model.Shareholder;
import ru.chernyavskiy.model.Stock;


public interface StockService {

    Stock findById(Long stockId);

    Iterable<Stock> listAll();

    void delete(Long id);

    Stock add(String name, Double currentValue);

    Stock edit(Long id, String name, Double currentValue);
}
