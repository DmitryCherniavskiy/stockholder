package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.dto.OrderDto;
import ru.chernyavskiy.model.Order;
import ru.chernyavskiy.repository.OrderRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ShareholderService shareholderService;

    @Autowired
    StockService stockService;

    @Override
    public List<OrderDto> findByShareholder(Long shareholderId) {
        List<Order> orderList = orderRepository.findByShareholderId(shareholderId);
        List<OrderDto> orderDtoList = new ArrayList<>();
        for (Order order: orderList){
            OrderDto tempOrderDto = new OrderDto();
            tempOrderDto.setOrderId(order.getOrderId());
            tempOrderDto.setShareholder(order.getShareholder());
            tempOrderDto.setStock(order.getStock());
            tempOrderDto.setPurchasePrise(order.getPurchasePrise());
            tempOrderDto.setPurchaseNumber(order.getPurchaseNumber());
            tempOrderDto.setPurchaseDate(order.getPurchaseDate());
            orderDtoList.add(tempOrderDto);
        }
        return orderDtoList;
    }

    @Override
    public Order findById(Long stockId) {
        return orderRepository.findById(stockId).get();
    }

    @Override
    public Iterable<Order> listAll() {
        return orderRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        orderRepository.delete(orderRepository.findById(id).get());
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }
}
