package ru.chernyavskiy.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.model.Shareholder;
import ru.chernyavskiy.model.Stock;
import ru.chernyavskiy.repository.StockRepository;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository stockRepository;

    @Override
    public Stock findById(Long stockId) {
        return stockRepository.findById(stockId).get();
    }

    @Override
    public Iterable<Stock> listAll() {
        return stockRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        stockRepository.delete(stockRepository.findById(id).get());
    }

    @Override
    public Stock add(String name, Double current) {
        return stockRepository.save(new Stock(name, current));
    }

    @Override
    public Stock edit(Long id, String name, Double currentValue) {
        Stock stock = stockRepository.findById(id).get();
        stock.setName(name);
        stock.setCurrentValue(currentValue);
        return stockRepository.save(stock);
    }


}
