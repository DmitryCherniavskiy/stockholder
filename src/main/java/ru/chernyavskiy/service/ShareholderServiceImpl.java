package ru.chernyavskiy.service;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.model.Shareholder;
import ru.chernyavskiy.repository.ShareholderRepository;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ShareholderServiceImpl implements ShareholderService {

    @Autowired
    private StockService stockService;

    @Autowired
    private ShareholderRepository shareholderRepository;

    @Override
    public Shareholder findById(Long id){
        return shareholderRepository.findById(id).get();
    }

    @Override
    public Iterable<Shareholder> listAll() {
        return shareholderRepository.findAll();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        shareholderRepository.deleteById(id);
    }

    @Override
    public Shareholder add(String firstName, String secondName, String lastName) {
        return shareholderRepository.save(new Shareholder(firstName, secondName, lastName));
    }

    @Override
    public Shareholder edit(Long id, String firstName, String secondName, String lastName) {
        Shareholder shareholder = shareholderRepository.findById(id).get();
        shareholder.setFirstName(firstName);
        shareholder.setSecondName(secondName);
        shareholder.setLastName(lastName);
        return shareholderRepository.save(shareholder);
    }



}
