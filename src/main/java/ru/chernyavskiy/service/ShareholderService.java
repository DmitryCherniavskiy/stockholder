package ru.chernyavskiy.service;

import ru.chernyavskiy.model.Shareholder;

public interface ShareholderService {

    Shareholder findById(Long id);

    Iterable<Shareholder> listAll();

    void delete(Long id);
    
    Shareholder add(String firstName, String secondName, String lastName);

    Shareholder edit(Long id, String firstName, String secondName, String lastName);

}
