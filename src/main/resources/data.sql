/*insert into "shareholder" ("shareholder_uid" ,"first_name", "second_name", "last_name") values (1,'Dmitry', 'Sviatoslavovich', 'Cherniavskiy');
insert into "shareholder" ("shareholder_uid" ,"first_name", "second_name", "last_name") values (2, 'Aleksey', 'Igorevich', 'Obuhov');
insert into "shareholder" ("shareholder_uid" ,"first_name", "second_name", "last_name") values (3, 'Maksim', 'Alexeevich', 'Smirnov');

insert into "stock" ("stock_uid", "name", "current_value") values (1, 'Sber', 2);
insert into "stock" ("stock_uid", "name", "current_value") values (2, 'Tinkoff', 3);
insert into "stock" ("stock_uid", "name", "current_value") values (3, 'Tesla', 4);
insert into "stock" ("stock_uid", "name", "current_value") values (4, 'Gazprom', 5);
insert into "stock" ("stock_uid", "name", "current_value") values (5, 'American Airlines', 6);

insert into "order" ("order_uid","shareholder_id","stock_id", "purchase_price", "purchase_number")
values(1, 1, 1, 2, 3 );
insert into "order" ("order_uid","shareholder_id","stock_id", "purchase_price", "purchase_number")
values(2, 1, 2, 2, 3 );
insert into "order" ("order_uid","shareholder_id","stock_id", "purchase_price", "purchase_number")
values(3, 1, 3, 2, 3 );
insert into "order" ("order_uid","shareholder_id","stock_id", "purchase_price", "purchase_number")
values(4, 2, 4, 2, 3 );
insert into "order" ("order_uid","shareholder_id","stock_id", "purchase_price", "purchase_number")
values(5 , 3, 5, 2, 3 );

insert into "user" ("login", "hash") values ('Admin','$2a$12$p7P36dPZUR7Jl/J7dCBU.eBS.PjDuvq/8/1ND13ZniMRc2uZTSJoe');
*/