var app = angular.module('stocks', []);

app.controller("StocksController", function ($scope, $http) {

    $scope.getStocks = function () {
        $http.get('/public/rest/stocks').success(function (data, status, headers, config) {
            $scope.stocksList = data;
        }).error(function (data, status, headers, config) {
            if (data.message === 'Time is out') {
                $scope.finishByTimeout();
            }
        });
    };

    $scope.getStocksByShareholder = function (id) {
        $http.get('/public/rest/stocks/'+id).success(function (data, status, headers, config) {
            $scope.stocksList = data;
        }).error(function (data, status, headers, config) {
            if (data.message === 'Time is out') {
                $scope.finishByTimeout();
            }
        });
    };

    $scope.deleteStock = function (id) {
        const formData = new FormData();
        formData.append('id', id);
        $http({
            url: '/public/rest/stocks',
            method: "DELETE",
            data: formData
        }).success(function (data, status, headers, config) {
            for (var i = 0; i < $scope.stocksList.length; i++) {
                var j = $scope.stocksList[i];
                if (j.id === id) {
                    $scope.stocksList.splice(i, 1);
                    break;
                }
            }
        }).error(function (data, status, headers, config) {
            console.error(status, data, headers);
            $scope.error = "���������� ������� �����, ������� ������� �������.";
        });
    };

    $scope.addStock = function () {
        var stockData = JSON.stringify({stockName:$scope.stockName, currentValue:$scope.currentValue});
        $http.post('/public/rest/stocks/', stockData).success(function (data, status, headers, config) {
            $scope.stocksList.splice(0, 0, data);
            $scope.getStocks();
        }
        ).error(function (data, status, headers, config) {
            console.error(status, data, headers);
        });

    };

    $scope.editStock = function () {
        var stockData = JSON.stringify({id:$scope.editId, stockName:$scope.editName, currentValue:$scope.editCurrentValue});
        $http({
            url: '/public/rest/stocks',
            method: "PUT",
            data: stockData
        }).success(function (data, status, headers, config) {
                $scope.stocksList.splice(0, 0, data);
                $scope.getStocks();
            }
        ).error(function (data, status, headers, config) {
            console.error(status, data, headers);
        });

    };

    $scope.addToEdit = function (id, name, currentValue){
        $scope.editId = id;
        $scope.editName = name;
        $scope.editCurrentValue = currentValue;
    }
});
