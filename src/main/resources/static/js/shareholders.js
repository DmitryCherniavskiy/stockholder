var app = angular.module('shareholders', []);

app.controller("ShareholdersController", function ($scope, $http) {

    $scope.getShareholders = function () {
        $http.get('/public/rest/shareholders').success(function (data, status, headers, config) {
            $scope.shareholdersList = data;
        }).error(function (data, status, headers, config) {
            if (data.message === 'Time is out') {
                $scope.finishByTimeout();
            }
        });
    };

    $scope.deleteShareholder = function (id) {
        const formData = new FormData();
        formData.append('id', id);
        $http({
            url: '/public/rest/shareholders',
            method: "DELETE",
            data: formData
        }).success(function (data, status, headers, config) {
            for (var i = 0; i < $scope.shareholdersList.length; i++) {
                var j = $scope.shareholdersList[i];
                if (j.id === id) {
                    $scope.shareholdersList.splice(i, 1);
                    break;
                }
            }
            $scope.getShareholders();
        }).error(function (data, status, headers, config) {
            console.error(status, data, headers);
            $scope.error = "Невозможно удалить владельца с имеющимися акциями";
        });
    };

    $scope.addShareholder = function () {
        var shareholderData = JSON.stringify({firstName:$scope.firstName, secondName:$scope.secondName, lastName:$scope.lastName});
        $http({
            url: '/public/rest/shareholders/',
            method: "POST",
            data: shareholderData
        }).success(function (data, status, headers, config) {
            $scope.shareholdersList.splice(0, 0, data);
            $scope.getShareholders();
        }
        ).error(function (data, status, headers, config) {
            console.error(status, data, headers);
        });

    };

    $scope.editShareholder = function () {
        var shareholderData = JSON.stringify({id:$scope.editId, firstName:$scope.editFirstName, secondName:$scope.editSecondName, lastName:$scope.editLastName});
        $http({
            url: '/public/rest/shareholders',
            method: "PUT",
            data: shareholderData
        }).success(function (data, status, headers, config) {
                $scope.shareholdersList.splice(0, 0, data);
                $scope.getShareholders();
            }
        ).error(function (data, status, headers, config) {
            console.error(status, data, headers);
        });

    };

    $scope.addToEdit = function (id, firstName,secondName, lastName){
        $scope.editId = id;
        $scope.editFirstName = firstName;
        $scope.editSecondName = secondName;
        $scope.editLastName = lastName;
    }

    $scope.redirectToShareholder = function (id){
        document.location='/orders/shareholder/'+id;
    }
});
